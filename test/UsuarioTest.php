<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Models\UsuarioModel;
use Models\Database;
final class UsuarioTest extends TestCase
{
   
    private $op;

    public function setUp(): void {
        
        new Database();
        $this->op = new Usuario;
    }

    public function testValidarUsuarioLoginInexistente(){
        $usuario = "prueba";
        $dataUser = UsuarioModel::where('email',$usuario)->get()->toArray();
        $this->assertEmpty($dataUser);

    }
}