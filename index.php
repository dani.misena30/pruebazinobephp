<?php
  //carga dependencias de composer
  require './vendor/autoload.php';
  
  //configuracion de entorno
  require  './core/config.php';

  //inicializa base datos con eloquent
  require  './core/dataBase.php';

  use Models\Database;

  new Database();
   
  //carga los modelos
  foreach(glob("./models/*.php") as $file){
    require $file;
  }

  //Base para los controladores
  require './core/controladorBase.php';

  //Funciones para el controlador frontal
  require './core/controladorFrontal.php';

  //Cargamos controladores y acciones
  if(isset($_GET["controller"])){
    $controllerObj=cargarControlador($_GET["controller"]);
    lanzarAccion($controllerObj);
  }else{
    $controllerObj=cargarControlador(CONTROLADOR_DEFECTO);
    lanzarAccion($controllerObj);
  }
  
  
