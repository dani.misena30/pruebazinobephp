<?php
  use Models\UsuarioModel;
  
  class Usuario extends ControladorBase {

    public function index() {
     
      session_start();
      if(empty($_SESSION['user'])) {
        $this->redirect('usuario/login');
      }
      
      $this->view('index' , [
       "base_url" => BASE_URL,
       "usuarioLogueado" => $_SESSION['user'],
       "idUserLogueado" => $_SESSION['id_user']
      ]);
    }

    public function login() {
      session_start();
      if(!empty($_SESSION['user'])) {
        $this->redirect('usuario/index');
      }
      
      $this->view('login' , [
        "base_url" => BASE_URL,
        "errorMsj" => ''
      ]);
    }

    public function validateLogin() {
      $user = $_POST['usuario'];
      $password = $_POST['password'];
      $dataUser = UsuarioModel::where('email',$user)->get()->toArray();
      $errorMsj = '';
      if(count($dataUser) == 0 || !filter_var($user, FILTER_VALIDATE_EMAIL)) {
        $errorMsj = "Usuario incorrecto";
      }elseif(!$this->validate_password($password ,$dataUser[0]['password'])) {
        $errorMsj = "Contraseña incorrecta";
      }

      if(empty($errorMsj)) {
        session_start();
        
        $_SESSION['user'] = $dataUser[0]['nombre'];

        $_SESSION['id_user'] = $dataUser[0]['id'];

        $this->redirect('usuario/index');
      }else {
        
        $this->view('login' , [
          "base_url" => BASE_URL ,
          "errorMsj" => $errorMsj
        ]);
      }
    }

    private function validate_password($password,$passwordBD) {
      $hash =sha1($password);
      if ($passwordBD==$hash) {
          return true;
      } else {
          return false;
      }
    }

    public function logout() {
      session_start();
      session_destroy();

      $this->redirect('usuario/login');
    }

    public function registro() {
      session_start();
      if(!empty($_SESSION['user'])) {
        $this->redirect('usuario/index');
      }
      
      $alertas = [];
      if(!empty($_POST)) {
        
        if(strlen($_POST['nombre'])  < 3 ) {
          $alertas[] = "El nombre debe tener minimo 3 caracteres";
        }

        if(strlen($_POST['password'])  < 6 ) {
          $alertas[] = "La contraseña debe tener minimo 6 caracteres";
        }

        if(preg_match ("/^[a-zA]+$/", $_POST['password'])) {
          $alertas[] = "La contraseña debe contener un digito";
        }

        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
          $alertas[] = "Email invalido";
        }

        $queryUsuarioCount = UsuarioModel::where('documento',$_POST['documento'])->orwhere('email',$_POST['email'])->count();

        if($queryUsuarioCount > 0) {
          $alertas[] = "Este usuario ya existe";
        }

        if(empty($alertas)) {
          $save = UsuarioModel::create([
            'nombre'=>$_POST['nombre'],
            'email'=>$_POST['email'],
            'documento'=>$_POST['documento'],
            'pais'=>$_POST['pais'],
            'password'=>sha1($_POST['password']),
            'titulo_profesional'=>$_POST['titulo'],
            'telefono'=>$_POST['telefono']
          ]);
          
          if($save){
            $alertas[] = "Usuario creado con exito";
          }
        }
      }

      $this->view('registro' , [
        "base_url" => BASE_URL,
        "file_url" => FILE_URL,
        'alertas' => $alertas
      ]);
    }


  }