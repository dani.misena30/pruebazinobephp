<?php 
	class CustomerData{


		public function apiObtenerPaises() {
			$requestPaises = file_get_contents('https://restcountries.eu/rest/v2/all');

			$arrayPaises = json_decode($requestPaises);       
			$jsonPaises = [];
			foreach($arrayPaises as $pais) {
				$jsonPaises[] =  $pais->name;
			}
			echo json_encode($jsonPaises);
		}

		public function apiUsuarios() {
		
			$documento = $_GET['busqueda'];
			
			$requestUsuariosUno = file_get_contents('http://www.mocky.io/v2/5d9f38fd3000005b005246ac?mocky-delay=10s');
			$respuestaUsuariosUno = json_decode($requestUsuariosUno);

			$requestUsuariosDos = file_get_contents('http://www.mocky.io/v2/5d9f39263000005d005246ae?mocky-delay=10s');
			$respuestaUsuariosDos = json_decode($requestUsuariosDos);

			$arregloUsuarioFiltrado = [];
			foreach((array)$respuestaUsuariosDos as $user) {
				foreach($user as $value){
					
					if($value->document == $documento){
						$arregloUsuarioFiltrado['nombre'] = $value->first_name . " " . $value->last_name;
						$arregloUsuarioFiltrado['documento'] = $value->document;
						$arregloUsuarioFiltrado['pais'] = $value->country;
						$arregloUsuarioFiltrado['email'] = $value->email;
						$arregloUsuarioFiltrado['telefono'] = $value->phone_number;
						$arregloUsuarioFiltrado['tituloProfesional'] = $value->job_title;
					}
					
				}
			}

			if(count($arregloUsuarioFiltrado) == 0) {
				foreach((array)$respuestaUsuariosUno as $user) {
					foreach($user as $value){
						
						if($value->cedula == $documento){
							$arregloUsuarioFiltrado['nombre'] = $value->primer_nombre . " " . $value->apellido;
							$arregloUsuarioFiltrado['documento'] = $value->cedula;
							$arregloUsuarioFiltrado['pais'] = $value->pais;
							$arregloUsuarioFiltrado['email'] = $value->correo;
							$arregloUsuarioFiltrado['telefono'] = $value->telefono;
							$arregloUsuarioFiltrado['tituloProfesional'] = $value->cargo;
						}
						
					}
				}
			}

			echo json_encode($arregloUsuarioFiltrado);
		}

		public function apiBusquedaUsuariosEmailNombre(){
			$busqueda = $_GET['busqueda'];
			
			$requestUsuariosUno = file_get_contents('http://www.mocky.io/v2/5d9f38fd3000005b005246ac?mocky-delay=10s');
			$respuestaUsuariosUno = json_decode($requestUsuariosUno);

			$requestUsuariosDos = file_get_contents('http://www.mocky.io/v2/5d9f39263000005d005246ae?mocky-delay=10s');
			$respuestaUsuariosDos = json_decode($requestUsuariosDos);

			$arregloUsuarioFiltrado = [];
			foreach((array)$respuestaUsuariosDos as $user) {
				foreach($user as $value){
					$nombre = $value->first_name . " " . $value->last_name;
					$email = $value->email;
					$primerNombre  = $value->first_name;
					$apellido  = $value->last_name;
					if($nombre == $busqueda || $email == $busqueda || $primerNombre == $busqueda || $apellido == $busqueda){
						$arregloUsuarioFiltrado['nombre'] = $value->first_name . " " . $value->last_name;
						$arregloUsuarioFiltrado['documento'] = $value->document;
						$arregloUsuarioFiltrado['pais'] = $value->country;
						$arregloUsuarioFiltrado['email'] = $value->email;
						$arregloUsuarioFiltrado['telefono'] = $value->phone_number;
						$arregloUsuarioFiltrado['tituloProfesional'] = $value->job_title;
						$arregloUsuarioFiltrado['id'] = $value->id;
						$arregloUsuarioFiltrado['departamento'] = $value->state;
						$arregloUsuarioFiltrado['ciudad'] = $value->city;
						
					}
					
				}
			}

			if(count($arregloUsuarioFiltrado) == 0) {
				foreach((array)$respuestaUsuariosUno as $user) {
					foreach($user as $value){
						
						$nombre = $value->primer_nombre . " " . $value->apellido;
						$email = $value->correo;
						$primerNombre  = $value->primer_nombre;
						$apellido  = $value->apellido;
						if($nombre == $busqueda || $email == $busqueda || $primerNombre == $busqueda ||  $apellido == $busqueda){
							$arregloUsuarioFiltrado['nombre'] = $value->primer_nombre . " " . $value->apellido;
							$arregloUsuarioFiltrado['documento'] = $value->cedula;
							$arregloUsuarioFiltrado['pais'] = $value->pais;
							$arregloUsuarioFiltrado['email'] = $value->correo;
							$arregloUsuarioFiltrado['telefono'] = $value->telefono;
							$arregloUsuarioFiltrado['tituloProfesional'] = $value->cargo;
							$arregloUsuarioFiltrado['id'] = $value->id;
						  $arregloUsuarioFiltrado['departamento'] = $value->departamento;
						  $arregloUsuarioFiltrado['ciudad'] = $value->ciudad;
						}
						
					}
				}
			}

			echo json_encode($arregloUsuarioFiltrado);
		}

	}