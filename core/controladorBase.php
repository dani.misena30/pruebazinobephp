<?php
 
 class ControladorBase {

    public function __construct() {
      ob_start ();
    }

    public function view($vista , $parametros = []){
      $loader = new \Twig\Loader\FilesystemLoader(FILE_URL .'/views');
      $twig = new \Twig\Environment($loader);
      
      echo $twig->render($vista . '.twig', $parametros);
    }

    public function redirect($url) {
      $urlFinal = BASE_URL . $url;
      echo "<script type='text/javascript'> document.location = '". $urlFinal ."'; </script>";
    }
 }
