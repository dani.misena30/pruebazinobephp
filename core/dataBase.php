<?php
  namespace Models; 
  use Illuminate\Database\Capsule\Manager as Capsule;
    
  class Database {
    private $DBDRIVER = 'mysql';
    private $DBHOST = 'localhost';
    private $DBNAME = 'bd_prueba_zinobe_php';
    private $DBUSER = 'root';
    private $DBPASS = '';

    function __construct() {
      $capsule = new Capsule;
      $capsule->addConnection([
        'driver' => $this->DBDRIVER,
        'host' => $this->DBHOST,
        'database' => $this->DBNAME,
        'username' => $this->DBUSER,
        'password' => $this->DBPASS,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
      ]);
      
      $capsule->bootEloquent();
    }
  }