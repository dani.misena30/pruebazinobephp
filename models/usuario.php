<?php
 
  namespace Models;
  
  use \Illuminate\Database\Eloquent\Model;
 
  class UsuarioModel extends Model {
      
    protected $table = 'zinobe_usuario';
    protected $fillable = ['nombre', 'email' , 'documento' , 'pais' , 'password' , 'titulo_profesional' , 'telefono'];
  }
 
?>